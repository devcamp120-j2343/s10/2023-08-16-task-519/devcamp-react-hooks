import { useEffect, useState } from "react";

const BasicForm = () => {
    const [firstName, setFirstName] = useState(localStorage.getItem("firstname") || "");
    const [lastName, setLastName] = useState(localStorage.getItem("lastname") || "");

    const onInputFirstNameChangeHandler = (event) => {
        setFirstName(event.target.value);
    }

    const onInputLastNameChangeHandler = (event) => {
        setLastName(event.target.value);
    }

    useEffect(() => {
        document.title = lastName + " " + firstName;

        localStorage.setItem("firstname", firstName);
        localStorage.setItem("lastname", lastName);
    }, [firstName, lastName]);

    return (
        <div>
            <input value={firstName} onChange={onInputFirstNameChangeHandler}></input>
            <br/>
            <input value={lastName} onChange={onInputLastNameChangeHandler}></input>
            <br/>
            <p>{lastName} {firstName}</p>
        </div>
    )
}

export default BasicForm;