import { useEffect, useState } from "react";

const CountClick = () => {
    const [count, setCount] = useState(0);

    const onBtnCountClick = () => {
        setCount(count + 1);
    }

    // Component did mount
    useEffect(() => {
        console.log("Hook1 called!");

        return () => {
            // Component will unmount
        }
    }, []);

    // Component did mount + Component did update
    useEffect(() => {
        console.log("Hook2 called!");
    });

    // Component did mount + Component did update + count update
    useEffect(() => {
        console.log("Hook3 called!");
    }, [count]);

    useEffect(() => {
        // document.title = `You clicked ${count} times`;
    }, [count])

    return (
        <div>
            <button onClick={onBtnCountClick}>Click me!</button>
            <p>You clicked {count} times</p>
        </div>
    )
}

export default CountClick;
