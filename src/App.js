import BasicForm from "./components/BasicForm";
import CountClick from "./components/CountClick";

function App() {
  return (
    <div style={{margin: "50px"}}>
      <div style={{margin: "20px"}}>
        <span>519.10</span>

        <CountClick/>
      </div>
      <div style={{margin: "20px"}}>
        <span>519.20</span>

        <BasicForm />
      </div>
    </div>
  );
}

export default App;
